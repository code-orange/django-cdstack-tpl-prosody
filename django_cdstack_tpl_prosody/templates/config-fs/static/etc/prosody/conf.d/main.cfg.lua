plugin_paths = { "/usr/share/jitsi-meet/prosody-plugins/" }

-- domain mapper options, must at least have domain base set to use the mapper
muc_mapper_domain_base = "{{ jitsi_meet_domain }}";

turncredentials_secret = "{{ turn_secret }}";

turncredentials = {
  { type = "stun", host = "{{ jitsi_meet_domain }}", port = "4446" },
  { type = "turn", host = "{{ jitsi_meet_domain }}", port = "4446", transport = "udp" },
  { type = "turns", host = "{{ jitsi_meet_domain }}", port = "443", transport = "tcp" }
};

cross_domain_bosh = false;
consider_bosh_secure = true;

VirtualHost "{{ jitsi_meet_domain }}"
        -- enabled = false -- Remove this line to enable this host
        authentication = "token"
        -- Properties below are modified by jitsi-meet-tokens package config
        -- and authentication above is switched to "token"
        app_id="{{ jitsi_meet_token_app_id }}"
        app_secret="{{ jitsi_meet_token_app_secret }}"
        -- Assign this host a certificate for TLS, otherwise it would use the one
        -- set in the global section (if any).
        -- Note that old-style SSL on port 5223 only supports one certificate, and will always
        -- use the global one.
        ssl = {
                key = "/etc/ssl/private/host.key";
                certificate = "/etc/ssl/certs/host.pem";
        }
        speakerstats_component = "speakerstats.{{ jitsi_meet_domain }}"
        conference_duration_component = "conferenceduration.{{ jitsi_meet_domain }}"
        -- we need bosh
        modules_enabled = {
            "bosh";
            "pubsub";
            "ping"; -- Enable mod_ping
            "speakerstats";
            "turncredentials";
            "conference_duration";
        }
        c2s_require_encryption = false

Component "conference.{{ jitsi_meet_domain }}" "muc"
    storage = "memory"
    modules_enabled = {
        "muc_meeting_id";
        "muc_domain_mapper";
        -- "token_verification";
    }
    admins = { "focus@auth.{{ jitsi_meet_domain }}" }
    muc_room_locking = false
    muc_room_default_public_jids = true

-- internal muc component
Component "internal.auth.{{ jitsi_meet_domain }}" "muc"
    storage = "memory"
    modules_enabled = {
      "ping";
    }
    admins = { "focus@auth.{{ jitsi_meet_domain }}", "jvb@auth.{{ jitsi_meet_domain }}" }
    muc_room_locking = false
    muc_room_default_public_jids = true

VirtualHost "auth.{{ jitsi_meet_domain }}"
    ssl = {
			key = "/etc/prosody/certs/prosody.key";
			certificate = "/etc/prosody/certs/prosody.crt";
    }
    authentication = "internal_plain"

Component "focus.{{ jitsi_meet_domain }}"
    component_secret = "{{ jitsi_meet_focus_secret }}"

Component "speakerstats.{{ jitsi_meet_domain }}" "speakerstats_component"
    muc_component = "conference.{{ jitsi_meet_domain }}"

Component "conferenceduration.{{ jitsi_meet_domain }}" "conference_duration_component"
    muc_component = "conference.{{ jitsi_meet_domain }}"
